/*!	@file warning.cpp
	@brief System for reporting errors and warnings (implementation)
*/

#include "warning.h"
#include <string>
#include <iostream>

using namespace std;

bool colored_output = true;

void set_colored(bool colored)
{
	colored_output = colored;
}

void print_msg(string function, int line, string fname, string message)
{
	if (function.size() > 0)
		cerr << "(" << function << ") [" << fname << ":" << line << "] " << message << endl;
	else
		cerr << message << endl;
}

void debug(string function, int line, string fname, string message)
{
	if (colored_output)
	{
		cerr << blue << bold << "Debug: " << off;
		print_msg(function, line, fname, message);
	} else
	{
		cerr << "DEBUG: ";
		print_msg(function, line, fname, message);
	}
}

void sdebug(string message)
{
	if (colored_output)
		cerr << blue << bold << "Debug: " << off << message << endl;
	else
		cerr << "DEBUG: " << message << endl;
}

void swarning(string message)
{
	if (colored_output)
		cerr << yellow << bold << "Warning: " << off << message << endl;
	else
		cerr << "WARNING: " << message << endl;
}

void error(string function, int line, string fname, string message)
{
	if (colored_output)
	{
		cerr << red << bold << "Error: " << off;
		print_msg(function, line, fname, message);
	} else
	{
		cerr << "ERROR: ";
		print_msg(function, line, fname, message);
	}
}

void warning(string function, int line, string fname, string message)
{
	if (colored_output)
	{
		cerr << yellow << bold << "Warning: " << off;
		print_msg(function, line, fname, message);
	} else
	{
		cerr << "WARNING: ";
		print_msg(function, line, fname, message);
	}
}

int test_warnings()
{
	error("test_warnings", __LINE__, __FILE__, "What?");
	warning("test_warnings", __LINE__, __FILE__, "What?");
	debug("test_warnings", __LINE__, __FILE__, "What?");
	return 0;
}
