#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <omp.h>
#include <boost/math/special_functions/erf.hpp>
#include "warning.h"

/*! @file main.cpp
	@brief The main source file performing simulations of the particle motion.
*/

using namespace std;
using namespace boost::math;

int internal_error = 0;			///< Indicates library's internal state (0 if no critical errors were observed so far)

double v = 0.2;

double omega = v;				///< Wave frequency \f$\omega\f$
double k     = 1.0;				///< Wave \f$k\f$
double a     = 0.05;			///< Wave amplitude
double v0    = 0.0;				///< Initial average velocity
double vth   = 1.0;				///< Initial thermal velocity
double nu    = 0.003;			///< Collisional frequency

const int counter_step = 200;	///< Number of counter steps between reports of a progress in a console
const double max_p = 100.0;		///< Report error if momenta go above this threshold

double ok = omega / k;			///< \f$\omega/k\f$

const double max_p_hist = 5.0;		///< Range of values \f$[-{\rm max}_p,+{\rm max}_p]\f$ plotted in a histogram
const int hist_n = 100;				///< Number of records in a histogram

// =====================================================================================================================

/*! @brief Hamiltonian flow of the dynamical system
*/

const bool standing_wave = false; ///< If set, the equations are solved in a laboratory frame with a standing wave;
                                  ///< otherwise equations are solved in a frame moving with a single travelling wave.

int get_d(double z, double p, double t, double &dz, double &dp)
{
    if (!standing_wave)
    {
        // Travelling wave in a moving frame
        dz = p - ok;
        dp = a*sin(k*z);
    } else
    {
        // Standing wave
        dz = p;
        dp = a*sin(omega*t)*sin(k*z);
    }
}

// =====================================================================================================================

/*! @brief Class that gathers time history of the particle ensemble.
*/

class statistic
{
public:
	int    records;			///< Number of records to store

	double *total_e;		///< Total energy in each "record"
	int    *particles;		///< Number of particles in each "record"

	statistic(int _records);
	~statistic();
};

statistic::statistic(int _records)
{
	records = _records;
	total_e = new double[records];
	particles = new int[records];
	for (int i = 0; i < records; i++)
	{
		total_e[i] = 0.0;
		particles[i] = 0;
	}
}

statistic::~statistic()
{
	delete[] total_e;
	delete[] particles;
}

// =====================================================================================================================

class bucket;

typedef void bucket_call(int i, void *data, bucket *obj);

/*! @brief A collection ("bucket") of particles
*/

class bucket
{
public:
	double t;						///< Local time
	double *z;						///< Coordinates of particles
	double *p;						///< Particle momenta
	int n;							///< Number of particles in the bucket

	int callbacks;					///< Total number of callbacks
	vector<void*> datas;			///< Data pointers
	vector<int> waits;				///< Wait times for each callback
	vector<bucket_call*> calls;		///< Callbacks

	int counter;					///< Number of steps elapsed
	double time_step;				///< A time step of a simulation

	/*! @param _n Number of particles in the bucket.
		@param _time_step Time step
	*/
	bucket(int _n, double _time_step);

	/*! @brief Reset the local time to 0
	*/
	void reset();

	/*! @brief Add a callback
		@param func Callback function
		@param wait Time in "ticks" between the callback executions
	*/
	void add_callback(bucket_call *func, void *data, int wait);

	/*! @brief Execute a simulation of particles in the bucket
		@param steps Number of steps
	*/
	void run(int steps);

	/*! @brief Perform a single time step
	*/
	void step();

	/*! @brief Saving coordinates and momenta of all particles
		@param name File name to save the data
	*/
	void save_points(char *name);

	/*! @brief Saving the particle density distribution
		@param name File name to save the data
		@param png_name File name to save a png image
		@param nphi Number of cells in 'phi'
		@param np Number of cells in 'p'
		@param max_p Maximum value of 'p' in the plot
		@param dphi Deviation in 'phi' (in number of cells) of each Gaussian
		@param dp Deviation in 'p' (in number of cells) of each Gaussian
	*/
	void save_distribution(const char *name, const char *png_name, int nphi = 100, int np = 100, double max_p = 4.0, double dphi = 1.0, double dp = 1.0);

	/*! @brief Saving the velocity histogram
		@param name File name to save the data
	*/
	void save_hist(const char *name);

	~bucket();
};

double mod2pi(double a)
{
	int n = floor(a/2/M_PI);
	a -= 2*M_PI*n;
	if (a < 0) a += 2*M_PI;
	return a;
}

void bucket::save_points(char *name)
{
	ofstream out(name, ofstream::out);
	out.precision(10);

	for (int i = 0; i < n; i++)
		out << mod2pi(k*z[i]) << " " << p[i] << endl;

	out.close();
}

void bucket::save_distribution(const char *name, const char *png_name, int nphi, int np, double max_p, double dphi, double dp)
{
	ofstream out(name, ofstream::out);
	out.precision(10);

	const double L = 4.0;
	double *data = new double[nphi*np];

	for (int i = 0; i < nphi*np; i++) data[i] = 0.0;

	int nx = int(L*dphi);
	int ny = int(L*dp);

	double C = 1.0/sqrt(2*M_PI)/sqrt(dphi)/sqrt(dp);

	for (int i = 0; i < n; i++)
	{
		if ((p[i] >= max_p) || (p[i] <= -max_p)) continue;
        // Floating point coordinates on a lattice
        double x = nphi*(mod2pi(k*z[i])/2/M_PI);
		double y = np*(p[i]/max_p/2 + 0.5);
        // Displacement from the cell corner
		double dx = x - floor(x);
		double dy = y - floor(y);
        // Replacing the particle with a Gaussian
        // Distributing particle density inside a square centered at the cell
		for (int a = -nx; a <= nx; a++)
			for (int b = -ny; b <= ny; b++)
				if ((a+floor(x) >= 0) && (a+floor(x) < nphi) && (b+floor(y) >= 0) && (b+floor(y) < np))
				{
					double rx = a + dx;
					double ry = b + dy;
					data[a+int(x)+(b+int(y))*nphi] += C*exp(-rx*rx/2/dphi/dphi-ry*ry/2/dp/dp);
				}
	}

    // Printing out the density profile
	for (int b = 0; b < np; b++)
	{
		for (int a = 0; a < nphi; a++)
		{
			double x = 2*M_PI*(double(a)/nphi);
			double y = 2*max_p*(double(b)/np - 0.5);
			out << x << " " << y << " " << data[a + b*nphi] << endl;
		}
		out << endl;
	}

	out.close();

    // Making a plot
    ofstream gp("tmp.gp", ofstream::out);
	gp << "set terminal png size 800,800; set output \"" << png_name << "\"; set pm3d map" << endl;
	gp << "set cbrange [0:70]" << endl;
	gp << "splot \"" << name << "\" using 1:2:3" << endl;
	gp.close();

	system("gnuplot tmp.gp");
}

void bucket::save_hist(const char *name)
{
	ofstream out(name, ofstream::out);
	out.precision(10);

	int *hist = new int[2*hist_n];
	for (int i = 0; i < 2*hist_n; i++) hist[i] = 0.0;

	for (int i = 0; i < n; i++)
	{
		int idx = int(p[i]/max_p_hist*hist_n + hist_n);
		if ((idx >= 0) && (idx < 2*hist_n)) hist[idx]++;
	}

	for (int i = 0; i < 2*hist_n; i++)
		out << (i-hist_n)*max_p_hist/hist_n << " " << hist[i] << endl;
	out.close();

	delete[] hist;
}

void bucket::add_callback(bucket_call *func, void *data, int wait)
{
	callbacks += 1;
	calls.push_back(func);
	datas.push_back(data);
	waits.push_back(wait);
}

void bucket::reset()
{
	t = 0.0;
	counter = 0;
}

void bucket::step()
{
	#pragma omp parallel for
	for (int i = 0; i < n; i++)
	{
		double dz1, dp1, dz2, dp2, dz3, dp3, dz4, dp4;
		double _p, _z, _t;

		_t = t;
		get_d(z[i], p[i], _t, dz1, dp1);
		_z = z[i] + dz1*time_step/2;
		_p = p[i] + dp1*time_step/2;
		_t += time_step/2;
		get_d(_z, _p, _t, dz2, dp2);
		_z = z[i] + dz2*time_step/2;
		_p = p[i] + dp2*time_step/2;
		get_d(_z, _p, _t, dz3, dp3);
		_z = z[i] + dz3*time_step;
		_p = p[i] + dp3*time_step;
		_t += time_step/2;
		get_d(_z, _p, _t, dz4, dp4);
		z[i] += (dz1 + 2*dz2 + 2*dz3 + dz4)*time_step/6;
		p[i] += (dp1 + 2*dp2 + 2*dp3 + dp4)*time_step/6;
	}

	double old_pth = 0.0;
	double old_p0 = 0.0;

	for (int i = 0; i < n; i++) old_p0 += p[i];
	old_p0 /= n;

	for (int i = 0; i < n; i++) old_pth += 2*(p[i]-old_p0)*(p[i]-old_p0);
	old_pth /= n;
	if (old_pth > max_p) { internal_error = 1; return; }

	for (int i = 0; i < n; i++)
	{
        // Adding "collisional" drag (towards the average velocity)
        // We assume that the majority of non-resonant particles stays at v0?
		p[i] -= nu*(p[i] - v0)*time_step;
        // Adding "collisional" velocity diffusion
		p[i] += sqrt(3*nu*time_step)*vth*(2.0*rand()/RAND_MAX-1.0);
	}

	t += time_step;
};

void bucket::run(int steps)
{
	for (int i = 0; i < steps; i++)
	{
		for (int k = 0; k < callbacks; k++)
		{
			if (counter % waits[k] == 0)
				calls[k](counter / waits[k], datas[k], this);
		}
		step();
		if (internal_error != 0) break;
		counter++;
	}
};

bucket::bucket(int _n, double _time_step)
{
	n = _n;
	time_step = _time_step;
	t = 0.0;
	z = new double[n];
	p = new double[n];
	callbacks = 0;
}

bucket::~bucket()
{
	delete[] z;
	delete[] p;
}

// =====================================================================================================================

string name;

const int particles = 50000*20;
const int print_steps = 50;
const double dt = 0.02;
const int steps = 20000*2;
const int e_num = 1000*2;
const int dist_num = 50;
const int hist_num = 50*2;

void print_progress(int i, void *data, bucket *obj)
{
	char message[256];
	sprintf(message, "%i out of %i", i+1, print_steps);
	sdebug(message);
}

ofstream *out_data;

void e_record(int i, void *data, bucket *obj)
{
	statistic *stat = (statistic*)data;

	double p0 = 0.0;
	for (int k = 0; k < obj->n; k++) p0 += obj->p[k];
	p0 /= obj->n;

	double *ptr = stat->total_e + i;
	for (int k = 0; k < obj->n; k++) *ptr += (obj->p[k]-p0)*(obj->p[k]-p0)/2.0;
	stat->particles[i] += obj->n;

	*out_data << obj->t << " " << *ptr << " " << obj->n << endl;
}

void dist_record(int i, void *data, bucket *obj)
{
	char num[256];
	sprintf(num, "%.3i", i);
	string out_name = "dist_" + name + "_" + string(num) + ".dat";
	string out_png  = "dist_" + name + "_" + string(num) + ".png";
	obj->save_distribution(out_name.c_str(), out_png.c_str());
}

void hist_record(int i, void *data, bucket *obj)
{
	char num[256];
	sprintf(num, "%.3i", i);
	string out_name = "hist_" + name + "_" + string(num) + ".dat";
	obj->save_hist(out_name.c_str());
}

int main(int argc, char **argv)
{
	if ((argc < 2) || (strlen(argv[1]) > 50))
	{
		error("main", __LINE__, __FILE__, "One command line parameter (output name) is required and it should not be longer than 50 symbols");
		return 1;
	}

	name = string(argv[1]);

	statistic stat(e_num);

	bucket ps(particles, dt);

	ps.add_callback(print_progress, NULL, steps/print_steps);
	ps.add_callback(e_record, (void*)&stat, steps/e_num);
	ps.add_callback(dist_record, NULL, steps/dist_num);
	ps.add_callback(hist_record, NULL, steps/hist_num);

	srand(1000);

	char name[256];
	sprintf(name, "%s.dat", argv[1]);
	out_data = new ofstream(name, ofstream::out);

	ps.reset();

	for (int i = 0; i < ps.n; i++)
	{
		double tmp = double(rand())/RAND_MAX;
		double _v = erf_inv(2*tmp-1.0);
		ps.p[i] = v0 + vth*_v;
		ps.z[i] = 2*M_PI*rand()/RAND_MAX;
	}
	ps.run(steps);

	out_data->close();

	return 0;
}
