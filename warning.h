#ifndef _WARNING_H_
#define _WARNING_H_
#include <string>

/*!	@file warning.h
	@brief System for reporting errors and warnings (definitions)
*/

using namespace std;

const string off = "\x1b[0m";
const string bold = "\x1b[1m";
const string black = "\x1b[30m";
const string red = "\x1b[31m";
const string green = "\x1b[32m";
const string yellow = "\x1b[33m";
const string blue = "\x1b[34m";
const string magenta = "\x1b[35m";
const string cyan = "\x1b[36m";
const string white = "\x1b[37m";

/*!
	@brief Prints an 'error' message (with color if 'colored' flag is set to true)

	@param function Function name
	@param line Line number
	@param fname File name
	@param message Message to print
*/
void error(string function, int line, string fname, string message);

/*!
	@brief Prints a 'warning' message (with color if 'colored' flag is set to true)

	@param function Function name
	@param line Line number
	@param fname File name
	@param message Message to print
*/
void warning(string function, int line, string fname, string message);

/*!
	@brief Prints a 'debug' message (with color if 'colored' flag is set to true)

	@param function Function name
	@param line Line number
	@param fname File name
	@param message Message to print
*/
void debug(string function, int line, string fname, string message);

/*!
	@brief Prints simple 'debug' message (with color if 'colored' flag is set to true)

	@param message Message to print
*/
void sdebug(string message);

/*!
	@brief Prints simple 'warning' message (with color if 'colored' flag is set to true)

	@param message Message to print
*/
void swarning(string message);

/*!
	@brief Sets a 'colored' flag

	@param colored 'Colored' flag value to set
*/
void set_colored(bool colored);

/*!
	@brief Testing the reporting system
*/
int test_warnings();

#endif