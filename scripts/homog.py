from math import *
from random import random

N = 1000000

c = 0.0
for i in range(N):
	d  = 2*random()-1.0
	c += d*d
print "<x^2>:", c/N
