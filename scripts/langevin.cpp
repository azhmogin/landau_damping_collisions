#include <cstdlib>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <random>

double nu = 0.1;
double c = sqrt(3.0)*sqrt(2.0);

double crandom()
{
	return 2.0*rand()/RAND_MAX - 1.0;
}

double evolve(double p, double dt, int n)
{
	for (int i = 0; i < n; i++)
	{
		p -= p*dt;
		p += c*crandom()*sqrt(dt);
	}
	return p;
}

int hist_n = 100;
double hist_z = 5.0;

int *hist;

void add(double x)
{
	int idx = int((x/hist_z + 1.0)*hist_n);
	if ((idx < 0) || (idx >= 2*hist_n))
		printf("Out of bounds...\n");
	else
		hist[idx] += 1;
}

int main()
{
	srand(1000);

	hist = new int[2*hist_n];
	for (int i = 0; i < 2*hist_n; i++) hist[i] = 0.0;

	int particles = 50000;
	int steps = 10000;
	double dt = 1e-2;
	for (int i = 0; i < particles; i++)
	{
		double p = crandom();
		double pp = evolve(p, dt, steps);
		add(pp);
	}

	FILE *dev = fopen("hist.dat", "wt");
	for (int i = 0; i < 2*hist_n; i++)
		fprintf(dev, "%lf %i\n", double(i-hist_n)/hist_n*hist_z, hist[i]);
	fclose(dev);

	return 0;
}


