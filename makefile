headers = .
libs = .

BIN = ./bin/
SRC = ./
COMP = g++
OPTS = -std=c++11 -I $(headers)

all: $(BIN)main

$(BIN)warning.o: $(SRC)warning.h $(SRC)warning.cpp
	$(COMP) $(OPTS) -c $(SRC)warning.cpp -o $@

$(BIN)main: $(SRC)main.cpp $(SRC)warning.h $(BIN)warning.o
	$(COMP) $(OPTS) -L $(libs) $(SRC)main.cpp $(BIN)warning.o -fopenmp -lpthread -o $@

